# Manuel Chichi's site

This repository contains all the content of `manuelchichi.com.ar`.

## Requirements
* [Hugo](https://github.com/gohugoio/hugo) >= v0.143.0

## Commands for local testing

```shell
# Initialize submodules
git submodule update --init --recursive
# This command will start the local server
hugo serve
# In a new tab this command will open the default browser with the page
xdg-open http://localhost:1313/
```

## Commands for building

```shell
# This command will build the website in /public/ folder
hugo
```

