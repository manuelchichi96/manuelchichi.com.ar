---
author: "Manuel Chichi"
title: "About me"
date: "2022-12-10"
hidemeta: true
searchHidden: true
ShowBreadCrumbs: false
---

Hi everyone!

I am Manuel Chichi an enthusiastic DevOps Engineer working for
[Avature](https://www.avature.net) (prev [Mikroways](https://mikroways.net/)). I am currently located in Argentina (GMT-3).
I speak both Spanish :es: and English :uk:.

In this blog you will find all my personal projects mostly related to technology
and DevSecOps. You can contact me via [e-mail](manuelchichi96@gmail.com), or
[linkedin](https://www.linkedin.com/in/manuel-chichi-b80732130/) if you also want
to see my experience, or 
[github](https://github.com/manuelchichi)/[gitlab](https://gitlab.com/manuelchichi96)
if you also want to see some of my work.

__Useful links:__

* [projects](/projects) where you will find all my current projects.
* [blog](/blog) where you will find all my posts.
