---
author: "Manuel Chichi"
title: "Introduction - Hello world"
date: "2022-12-10"
description: "This is my first post in this blog"
tags: ["hugo","static websites","introduction"]
hideMeta: false
searchHidden: false
ShowBreadCrumbs: false
---

# Introduction

## Why I am doing this?

I am going to use this website as a portfolio. The first project that I want to
document is my experience while creating a private cloud with
[OpenStack](https://www.openstack.org/). Last but not least, I want to get
better at writing in English :uk:.

## Why I have chosen static websites?
Static websites seem like a fitting idea to this type of portfolio because
I don't want to waste time setting up a complete environment such as with
Wordpress.

Here are the key reasons:
* Security: I don't want to have any service/backend that can be compromised.
* Performance: They are faster because you only have to serve static HTML files
  and scripts.
* Costs: The deployment of this website is free (only limited by the amount of
  requests determine by
  [cloudfare](https://developers.cloudflare.com/workers/platform/limits)).
* Simplicity: I don't want to have any private data stored on this website. I
  just want to write content.

## Why Hugo?
I have previous experience documenting with [Hugo](https://gohugo.io/). So that's
why I choose it over other options but I don't discard trying
[Jekyll](https://jekyllrb.com/) in the future. While I was looking for a theme
to use with Hugo I found a really practical one which is
[PaperMod](https://github.com/adityatelange/hugo-PaperMod). This theme has many
things I do like for a blog like the [archive](/archives) page and many other
features (you should check it out).

## How this site works?
All the content of this static website can be located in this
[repository](https://gitlab.com/manuelchichi96/manuelchichi.com.ar). Now it
contains a CI/CD pipeline that allows to automatic deploy new changes after a
push to the main branch. This site is currently beign hosted by
[Cloudfare](https://www.cloudflare.com/) using it's
[pages](https://developers.cloudflare.com/pages/) funtionality.

# Conclusion

Welcome aboard this journey! Hope you like it.
