---
author: "Manuel Chichi"
title: "Host setup - Installation part 1"
date: "2022-12-31"
description: ""
tags: ["private cloud","ubuntu","openstack"]
hideMeta: false
searchHidden: false
ShowBreadCrumbs: false
---

**Updated on 2025-02-01**

This is a continuation of
[my private cloud](/projects/private-cloud/entry-2022-12-11/) project.

# Ubuntu server

## Installing Ubuntu ISO on server

First I need to download an Ubuntu 24.04 ISO in order to install it on the host
server. This can be done from the following
[link](https://mirror.sitsa.com.ar/ubuntu-releases/24.04.1/ubuntu-24.04.1-live-server-amd64.iso).
The idea is to do an installation of this OS with the minimun required packages.
I am not going to enter into further details about how I did this instalation.

## Setting up the Host

### User creation

After the OS is installed from my main user I am going to create a new user and
add my public keys to it. Also I want to add this user to the sudoers group and
allow to execute sudo without a password (because we are not going to set one).

```sh
sudo su
adduser --disabled-password openstack
usermod -aG sudo openstack
visudo

# There add the following line at the end
openstack ALL=(ALL) NOPASSWD:ALL

# Add my public ssh key to authorized keys file.
echo "my_key" >> /home/openstack/.ssh/authorized_keys
```

I am going to add this server as an alias into my ssh config file
(`~/.ssh/config`) so it's easier to enter to my OpenStack instance.
```
Host mc-cloud-openstack-1
  Hostname 192.168.0.150
  User openstack
```

Now I can login to my server simply by typing `ssh mc-cloud-openstack-1`.

### Networking 

In order for OpenStack to work correctly in one host with only one network
device we need to make some changes to the netplan configuration to add a
bridge with two ethernet devices.

First let's create the bridge with a static ip by adding the following to our
`/etc/netplan/*.yaml` file. Also we are going to add the physical interface.

```
network:
  version: 2
  ethernets:
    enp4s0:
      dhcp4: no
  bridges:
    br0:
      addresses:
        - 192.168.0.150/24
      gateway4: 192.168.0.1
      nameservers:
        addresses: [192.168.0.1]
      interfaces:
        - enp4s0
```

Apply the plan.

```
sudo netplan apply
```

Now create the following file `/lib/systemd/network/25-veth-b1.netdev` with the
following content. This will create the a virtual ethernet device where veth1 is
one "end" of the device and veth2 is the other "end".

```
[NetDev]
Name=veth1
Kind=veth
[Peer]
Name=veth2
```

Now restart the systemd service.

```
sudo systemctl restart systemd-networkd
```

It's time to edit the `/etc/netplan/*.yaml` file by adding the new veths.
Remember to add one end of the veth to the bridge in order to connect it to
neutron network later.

```
network:
  version: 2
  ethernets:
    enp4s0:
      dhcp4: no
    veth1: {}
    veth2: {}
  bridges:
    br0:
      addresses:
        - 192.168.0.150/24
      gateway4: 192.168.0.1
      nameservers:
        addresses: [192.168.0.1]
      interfaces:
        - veth1
        - enp4s0
```

Apply the changes.

```
sudo netplan apply
```

Network is now ready for installing OpenStack.

# OpenStack installation

In the following steps I am going to install the required packages for deploying
OpenStack using [Kolla-Ansible](https://github.com/openstack/kolla-ansible).

First I am going to install `direnv` and `pyenv`.

```sh
sudo apt-get install direnv python3.10-venv python3-pip
```

Next we have to add direnv hooks to our shell and restart.

```sh
direnv hook bash >> ~/.bashrc
exit
```

Now back again in the host create a new directory to start working in the
deployment.

```
mkdir kolla-ansible && cd kolla-ansible
echo "layout python3" > .envrc
direnv allow
```

Install OpenStack requirements.

```sh
# Install openstack dependencies
sudo apt install git python3-dev libffi-dev gcc libssl-dev build-essential libdbus-1-dev libglib2.0-dev
# Create requirements.txt file
cat > requirements.txt << EOF
ansible-core>=2.16,<2.19
git+https://opendev.org/openstack/kolla-ansible@19.2.0
python-openstackclient==7.1.4
docker==7.1.0
dbus-python==1.3.2
EOF

# Install pip packages
pip3 install -r requirements.txt
```

Create kolla directory.

```sh
sudo mkdir /etc/kolla
sudo chown $USER:$USER /etc/kolla
```

Then we have to copy etc files and main inventory.

```sh
# Copy default etc kolla files
cp -r .direnv/python-3.xx/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
# Copy default all-in-one inventory
cp -r .direnv/python-3.xx/share/kolla-ansible/ansible/inventory/all-in-one .
```

Now we are going to configure globals file. We only need to set the following
values.

```sh
vim /etc/kolla/globals.yml
```

Values to change.

```yaml
kolla_base_distro: "ubuntu"
kolla_internal_vip_address: "192.168.0.151"
network_interface: "br0"
neutron_external_interface: "veth2"
```

Generate default passwords

```
kolla-genpwd
```

Install OpenStack using kolla-ansible.

```sh
kolla-ansible install-deps
kolla-ansible bootstrap-servers -i ./all-in-one 
kolla-ansible prechecks -i ./all-in-one 
kolla-ansible deploy -i ./all-in-one 
kolla-ansible post-deploy -i ./all-in-one 

# You can use this command if you want to initialize the admin project with some
# examples
.direnv/python-3.xx/share/kolla-ansible/init-runonce
```

Now we can access the gui using kolla internal vip address. For this you need to
extract the password from `/etc/kolla/passwords.yml`.

# Openstack resources

## External network

On the Openstack interface go to `Admin>Network`. There we are going to need a
external network with the following configuration in order to use our local
network to access the internet.

```
provider type: flat
physical network: physnet1
external network: enabled
```

## Router

We need to create a new router selecting our previously created network.

## Project and users

Create a new project and a new user to separate from the admin one. Once you
have a new project, everytime you create a new network that you need to access
the internet you can add an interface in the router.

# Conclusion

Let’s see how this goes in the
[next post](/projects/private-cloud/entry-2024-03-18/). You can also check the 
[previous one](/projects/private-cloud/entry-2022-12-11/) if you want.
